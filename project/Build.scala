import sbt._
import Keys._
 
object GameEngineBuild extends Build {
  val Organization = "com.yatchmenoff"
  val Version      = "0.1-SNAPSHOT"
  val ScalaVersion = "2.10.3"

  lazy val gameengine = Project(
    id = "gameengine",
    base = file("."),
    settings = defaultSettings ++ Seq(
      libraryDependencies ++= Seq(
        "org.lwjgl.lwjgl" % "lwjgl" % "2.9.0"
      )
    )
  )
 
  lazy val defaultSettings = Project.defaultSettings ++ Seq(
    organization := "com.yatchmenoff",
    version      := "0.1-SNAPSHOT",
    scalaVersion := "2.10.3",
    scalacOptions ++= Seq("-encoding", "UTF-8", "-deprecation", "-unchecked", "-feature"),
    javacOptions  ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")
  )
}

