package com.yatchmenoff.gameengine

import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import com.yatchmenoff.gameengine.math._

class MeshRenderer {
  def renderMesh(mesh: Mesh, transform: Matrix3x3): Unit = {
    glPushMatrix()
    glMultMatrix(RenderUtil.matrixBuffer(transform))
    renderMesh(mesh)
    glPopMatrix()
  }

  def renderMesh(mesh: Mesh): Unit = {
    glDisable(GL_TEXTURE_2D)
    glColor4f(1,1,1,1)

    mesh match {
      case MeshSeq(m :: rest) => {
        renderMesh(m)
        renderMesh(MeshSeq(rest))
      }
      case MeshSeq(nil) => ()
      case m: LineMesh => {
        glBegin(GL_LINES)
        for(vertex <- m.vertices) {
          glVertex2f(vertex.v.x, vertex.v.y)
        }
        glEnd()
      }
      case m: TriangleMesh => {
        glBegin(GL_TRIANGLES)
        for(vertex <- m.vertices) {
          glVertex2f(vertex.v.x, vertex.v.y)
        }
        glEnd()
      }
    }
  }
}

