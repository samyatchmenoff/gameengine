package com.yatchmenoff.gameengine

import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.{
  Display, 
  DisplayMode
}
import org.lwjgl.input.Keyboard
import Keyboard._
import com.yatchmenoff.gameengine.math._

object Context {
  var windowCount = 0
}

class Context {
  val meshRenderer = new MeshRenderer
  val textRenderer = new TextRenderer

  def createWindow(title: String, width: Int, height: Int) = {
    Context.windowCount += 1
    if(Context.windowCount > 1) {
      throw new RuntimeException("LWJGL doesn't support multiple windows yet")
    }
    new Window(this, title, width, height)
  }

  def isQuitRequested = Display.isCloseRequested
}

trait RenderJob {
  def viewport: Viewport
}

case class MeshRenderJob(viewport: Viewport, mesh: Mesh, transform: Matrix3x3) extends RenderJob
case class TextRenderJob(
  viewport: Viewport,
  text: String,
  font: Font,
  transform: Matrix3x3,
  alignment: Text.Alignment
) extends RenderJob
case class ClearJob(viewport: Viewport, color: Color) extends RenderJob

class Window(val context: Context, title: String, width: Int, height: Int) {
  val displayMode = new DisplayMode(width, height)
  Display.setTitle(title)
  Display.setDisplayMode(displayMode)
  Display.setVSyncEnabled(true);
  Display.create()

  glEnable(GL_BLEND)
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

  var renderJobs: Seq[RenderJob] = Seq()

  private[this] def setViewport(viewport: Viewport) = {
    glViewport(
      (viewport.x * width).toInt,
      (viewport.y * height).toInt,
      (viewport.width * width).toInt,
      (viewport.height * height).toInt
    )
  }

  def close() = {
    Display.destroy()
  }

  def flip() = {
    val aspect = width.toFloat / height
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-aspect, aspect, -1, 1, -1, 1)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    renderJobs foreach { job =>
      setViewport(job.viewport)
      executeJob(job)
    }
    Display.update()

    renderJobs = Seq()
  }

  def executeJob(job: RenderJob) = {
    job match {
      case ClearJob(viewport, color) => {
        glScissor(
          (viewport.x * width).toInt,
          (viewport.y * height).toInt,
          (viewport.width * width).toInt,
          (viewport.height * height).toInt
        )
        glEnable(GL_SCISSOR_TEST)
        glClearColor(color.r, color.g, color.b, color.a)
        glClear(GL_COLOR_BUFFER_BIT)
        glDisable(GL_SCISSOR_TEST)
      }
      case MeshRenderJob(_, mesh, transform) => {
        context.meshRenderer.renderMesh(mesh, transform)
      }
      case TextRenderJob(_, text, font, transform, alignment) => {
        context.textRenderer.renderText(text, font, transform, alignment)
      }
    }
  }

  def clear(viewport: Viewport, color: Color) = {
    renderJobs = renderJobs :+ ClearJob(viewport, color)
  }

  def renderMesh(viewport: Viewport, mesh: Mesh, transform: Matrix3x3) = {
    renderJobs = renderJobs :+ MeshRenderJob(viewport, mesh, transform)
  }

  def renderText(viewport: Viewport, text: String, font: Font, transform: Matrix3x3, alignment: Text.Alignment) = {
    renderJobs = renderJobs :+ TextRenderJob(viewport, text, font, transform, alignment)
  }
}

case class Viewport(x: Float, y: Float, width: Float, height: Float)

