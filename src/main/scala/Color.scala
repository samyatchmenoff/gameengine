package com.yatchmenoff.gameengine

object Color {
  val zero = Color(0, 0, 0, 0)
}

case class Color(r: Float, g: Float, b: Float, a: Float)
