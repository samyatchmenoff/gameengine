package com.yatchmenoff.gameengine

import com.yatchmenoff.gameengine.math._

case class Vertex(v: Vector2)

trait Mesh {
  def vertices: Seq[Vertex]
  def mapVectors(fn: Vector2 => Vector2): Mesh

  def +(o: Mesh) = MeshSeq(Seq(this, o))

  def translate(x: Float, y: Float) = mapVectors(v => v + Vector2(x, y))
  def scale(x: Float, y: Float) = mapVectors(v => Vector2(x * v.x, y * v.y))
}

case class MeshSeq(subMeshes: Seq[Mesh]) extends Mesh {
  def vertices = subMeshes.flatMap(_.vertices)
  def mapVectors(fn: Vector2 => Vector2): Mesh = {
    MeshSeq(subMeshes.map(_.mapVectors(fn)))
  }
}

case class TriangleMesh(vertices: Seq[Vertex]) extends Mesh {
  def mapVectors(fn: Vector2 => Vector2): Mesh = {
    TriangleMesh(vertices map {
      case Vertex(v) => Vertex(fn(v))
    })
  }
}

object TriangleMesh {
  val unitSquare = TriangleMesh(Seq(
    Vertex(Vector2(0, 0)), Vertex(Vector2(1, 0)), Vertex(Vector2(0, 1)),
    Vertex(Vector2(1, 1)), Vertex(Vector2(0, 1)), Vertex(Vector2(1, 0))
  ))
}

case class LineMesh(lines: Seq[(Vertex,Vertex)]) extends Mesh {
  def vertices = lines flatMap { case (a,b) => Seq(a,b) }
  def mapVectors(fn: Vector2 => Vector2): Mesh = {
    LineMesh(lines map {
      case (Vertex(a), Vertex(b)) => (Vertex(fn(a)), Vertex(fn(b)))
    })
  }
}

object LineMesh {
  val unitSquare = LineMesh(Seq(
    ( Vertex(Vector2(0, 0)), Vertex(Vector2(1, 0)) ),
    ( Vertex(Vector2(1, 0)), Vertex(Vector2(1, 1)) ),
    ( Vertex(Vector2(1, 1)), Vertex(Vector2(0, 1)) ),
    ( Vertex(Vector2(0, 1)), Vertex(Vector2(0, 0)) )
  ))
}

