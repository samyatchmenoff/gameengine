package com.yatchmenoff.gameengine.tanks

import scala.util.Random
import org.lwjgl.input.Keyboard
import com.yatchmenoff.gameengine._
import com.yatchmenoff.gameengine.math._
import LineMesh._
import Constants._

case class Damage(amount: Int)

object Projectile extends EntityType {
  case class State(
    owner: EntityRef[Tank.State],
    pos: Vector2,
    vel: Vector2,
    age: Float
  )

  def step(self: EntityRef[State], state: State, messages: Seq[AnyRef])(implicit ctx: StepContext): Option[State]= {
    val hits = ctx.entities.collect({
      case e @ Entity(tank: Tank.State) if e.ref != state.owner && (tank.pos - state.pos).mag < 1.0f => e
    }).toSeq

    hits match {
      case Nil => {
        (state.age - dt) match {
          case a if a > 0.0f => Some(state.copy(pos = state.pos + dt * state.vel, age = a))
          case _ => None
        }
      }
      case _ => {
        hits.foreach { e =>
          e.ref ! Damage(1)
        }
        None
      }
    }
  }

  def render(window: Window, viewport: Viewport, transform: Matrix3x3, state: State) = {
    val baseTransform = transform * Matrix3x3.translate(state.pos.x, state.pos.y)
    window.renderMesh(viewport, mesh, baseTransform)
  }

  val mesh = LineMesh.unitSquare.translate(-0.5f, -0.5f).scale(0.3f, 0.3f)
}

object Tank {
  case class State(
    pos: Vector2,
    rot: Float,
    aim: Float,
    reload: Float,
    life: Int
  )

  case class TankControl(vel: Float, turn: Float, aim: Float, shoot: Boolean)

  val gunMesh = unitSquare.translate(-0.5f, -1/8.0f).scale(0.3f, 1)
  val bodyMesh = {
    val middle = unitSquare.translate(-0.5f, -0.5f)
    val left   = unitSquare.translate(-0.5f, -0.5f).scale(0.5f, 1.5f).translate(-0.75f, 0)
    val right  = unitSquare.translate(-0.5f, -0.5f).scale(0.5f, 1.5f).translate( 0.75f, 0)
    (middle + left + right)
  }

  val textBoxMesh = unitSquare.translate(-0.5f, 0.0f).scale(5.0f, 1.0f)
  val font = Font("SansSerif", FontStyle.Plain, 64)

  def randomTank = {
    Tank.State(Vector2((Random.nextFloat-0.5f) * 40, (Random.nextFloat-0.5f) * 30), Tau * Random.nextFloat, 0, 0, 5)
  }
}

abstract class Tank extends EntityType {
  type State = Tank.State

  def stepTank(self: EntityRef[State], state: State, messages: Seq[AnyRef], input: Tank.TankControl)(implicit ctx: StepContext): Option[State] = {
    val canShoot = state.reload == 0.0f
    val newState = state.copy(
      pos = state.pos + 2.5f * dt * input.vel * (Matrix3x3.rotate(state.rot) * Vector2(0, 1)),
      rot = state.rot + 1.5f * dt * input.turn,
      aim = state.aim + 1.0f * dt * input.aim,
      reload = (state.reload - dt / 0.5f, canShoot, input.shoot) match {
        case (_, true, true) => 1.0f
        case (r, _, _) if r > 0.0f => r
        case _ => 0.0f
      },
      life = state.life - messages.collect({ case Damage(n) => n }).sum
    )

    (canShoot && input.shoot) match {
      case true => {
        ctx.createEntity(Projectile(Projectile.State(
          self,
          state.pos + Matrix3x3.rotate(state.rot+state.aim) * Vector2(0, 1) * 1.0f,
          Matrix3x3.rotate(state.rot+state.aim) * Vector2(0, 1) * 5.0f,
          2.0f
        )))
      }
      case false => ()
    }

    newState.life match {
      case n if n > 0 => Some(newState)
      case _ => None
    }
  }

  def render(window: Window, viewport: Viewport, transform: Matrix3x3, state: State) = {
    val baseTransform =
      transform *
      Matrix3x3.translate(state.pos.x, state.pos.y) *
      Matrix3x3.rotate(state.rot)

    window.renderMesh(viewport, Tank.bodyMesh, baseTransform)
    window.renderMesh(viewport, Tank.gunMesh, baseTransform * Matrix3x3.rotate(state.aim) * Matrix3x3.translate(0, -0.3f * state.reload))

    val textTransform = transform * Matrix3x3.translate(state.pos.x, state.pos.y + 1.0f) * Matrix3x3.scale(0.5f, 0.5f)
    window.renderMesh(viewport, Tank.textBoxMesh, textTransform)
    window.renderText(viewport, s"Health: ${state.life}", Tank.font, textTransform, Text.Alignment.CenterBottom)
  }
}

object PlayerTank extends Tank {
  import Tank._
  def step(self: EntityRef[State], state: State, messages: Seq[AnyRef])(implicit ctx: StepContext): Option[State] = {
    val vel = (ctx.input.keyState(Keyboard.KEY_W), ctx.input.keyState(Keyboard.KEY_S)) match {
      case (true, false) => 1.0f
      case (false, true) => -1.0f
      case _ => 0.0f
    }
    val turn = (ctx.input.keyState(Keyboard.KEY_D), ctx.input.keyState(Keyboard.KEY_A)) match {
      case (true, false) => -1.0f
      case (false, true) => 1.0f
      case _ => 0.0f
    }
    val shoot = ctx.input.events.exists(_ == KeyDown(Keyboard.KEY_SPACE))

    stepTank(self, state, messages, TankControl(vel, turn, 0, shoot))
  }
}

object JoyTank extends Tank {
  import Tank._
  def step(self: EntityRef[State], state: State, messages: Seq[AnyRef])(implicit ctx: StepContext): Option[State] ={
    val vel = -ctx.input.joyAxisState(0,1)
    val turn = -ctx.input.joyAxisState(0,0)
    val aim = -ctx.input.joyAxisState(0,2)
    val shoot = ctx.input.events.exists {
      case JoyButtonDown(_, b) => (4 until 20) contains b
      case _ => false
    }

    stepTank(self, state, messages, TankControl(vel, turn, aim, shoot))
  }
}

object AITank extends Tank {
  import Tank._
  def step(self: EntityRef[State], state: State, messages: Seq[AnyRef])(implicit ctx: StepContext): Option[State] = {
    stepTank(self, state, messages, TankControl(1.0f, 1.0f, 0, true))
  }
}
