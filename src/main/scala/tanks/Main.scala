package com.yatchmenoff.gameengine.tanks

import java.util.UUID.randomUUID
import scala.reflect.ClassTag
import scala.util.Random
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Controllers
import org.lwjgl.opengl.Display
import org.lwjgl.opengl.GL11._
import org.lwjgl.Sys
import com.yatchmenoff.gameengine._
import com.yatchmenoff.gameengine.math._
import LineMesh._

object Constants {
  val dt: Float = 1.0f/60.0f
}

object Entity {
  def unapply(e: Entity[_]): Option[Any] = Some(e.state)
}

abstract class Entity[T: ClassTag](val key: String, val state: T) {
  def step(ctx: StepContext, messages: Seq[AnyRef]): Option[Entity[T]]
  def render(window: Window, viewport: Viewport, transform: Matrix3x3)

  val ref = new EntityRef[T](key)
}

trait EntityType { self =>
  type State

  def step(self: EntityRef[State], state: State, messages: Seq[AnyRef])(implicit ctx: StepContext): Option[State]
  def render(window: Window, viewport: Viewport, transform: Matrix3x3, state: State)

  def apply(state: State)(implicit classTag: ClassTag[State]): Entity[State] = apply(randomUUID.toString, state)

  def apply(key: String, state: State)(implicit classTag: ClassTag[State]): Entity[State] = {
    new Entity[State](key, state) {
      def step(ctx: StepContext, messages: Seq[AnyRef]): Option[Entity[State]] = {
        val entityRef = new EntityRef[State](key)
        self.step(entityRef, state, messages)(ctx) map { s => self(key, s) }
      }

      def render(window: Window, viewport: Viewport, transform: Matrix3x3) = {
        self.render(window, viewport, transform, state)
      }
    }
  }
}

trait InputEvent
case class KeyDown(key: Int) extends InputEvent
case class KeyUp(key: Int) extends InputEvent
case class JoyButtonDown(joyIndex: Int, button: Int) extends InputEvent
case class JoyButtonUp(joyIndex: Int, button: Int) extends InputEvent

case class Input(events: Seq[InputEvent]) {
  def keyState(key: Int): Boolean = Keyboard.isKeyDown(key)
  def joyButtonState(joyIndex: Int, button: Int): Boolean = {
    Controllers.getController(joyIndex).isButtonPressed(button)
  }
  def joyAxisState(joyIndex: Int, axis: Int): Float = {
    Controllers.getController(joyIndex).getAxisValue(axis)
  }
}

case class EntityRef[T: ClassTag](val key: String) {
  def getState(implicit ctx: StepContext): Option[T] = {
    ctx.getEntity(key) match {
      case Some(Entity(state: T)) => Some(state)
      case _ => None
    }
  }

  def !(msg: AnyRef)(implicit ctx: StepContext) = {
    ctx.sendMessage(key, msg)
  }
}

class StepContext(gameState: GameState, val input: Input) {
  var newEntities: Map[String,Entity[_]] = Map()
  var messages: Seq[(String,AnyRef)] = Seq()

  def entities: Iterator[Entity[_]] = gameState.entities.valuesIterator
  def getEntity(key: String): Option[Entity[_]] = gameState.entities.get(key)
  def createEntity[T: ClassTag](entity: Entity[T]): EntityRef[T] = {
    newEntities = newEntities + (entity.key -> entity)
    entity.ref
  }
  def sendMessage(key: String, msg: AnyRef) = {
    messages = messages :+ (key,msg)
  }
}

case class GameState(entities: Map[String,Entity[_]])

object TanksApp extends App {
  val context = new Context
  val window = context.createWindow("Breakout", 1280, 720)
  val viewport = Viewport(0, 0, 1, 1)

  Controllers.create()

  var lastFPS = (Sys.getTime() * 1000) / Sys.getTimerResolution()
  var fps = 0

  val camTransform = Matrix3x3.scale(0.075f, 0.075f)

  var t = 0.0f

  val initialEntities = Seq(
    Seq(PlayerTank(Tank.State(Vector2(-10,0), Tau*0.75f, 0, 0, 5))),
    for(i <- 0 until Controllers.getControllerCount()) yield {
      JoyTank(Tank.State(Vector2(10, i), Tau*0.25f, 0, 0, 5))
    },
    for(_ <- 0 until 10 if Controllers.getControllerCount() == 0) yield {
      AITank(Tank.randomTank)
    }
  ).flatten

  var gameState = GameState(initialEntities.map(e => (e.key -> e)).toMap)

  var messages: Map[String,Seq[AnyRef]] = Map()

  while(!context.isQuitRequested) {
    var inputEvents: Seq[InputEvent] = Seq()
    while(Keyboard.next()) {
      if(Keyboard.getEventKeyState()) {
        inputEvents = inputEvents :+ KeyDown(Keyboard.getEventKey())
      } else {
        inputEvents = inputEvents :+ KeyUp(Keyboard.getEventKey())
      }
    }
    while(Controllers.next()) {
      if(Controllers.isEventButton()) {
        val controller = Controllers.getEventSource()
        val controlIndex = Controllers.getEventControlIndex()
        val value = controller.isButtonPressed(controlIndex)
        inputEvents = inputEvents :+ (value match {
          case true => JoyButtonDown(controller.getIndex(), controlIndex)
          case false => JoyButtonUp(controller.getIndex(), controlIndex)
        })
      }
    }
    val input = Input(inputEvents)

    var stepContext = new StepContext(gameState, input)
    def stepEntity(item: (String,Entity[_]), ctx: StepContext): Option[(String,Entity[_])] = {
      item match {
        case (k,v) => v.step(ctx, messages.get(k).getOrElse(Seq())).map(k -> _)
      }
    }
    gameState = GameState(
      gameState.entities.flatMap(stepEntity(_, stepContext)) ++ stepContext.newEntities
    )
    messages = stepContext.messages.groupBy(_._1).mapValues(_.map(_._2))

    window.clear(viewport, Color(0.1f, 0.05f, 0.03f, 0))
    gameState.entities.valuesIterator.foreach(_.render(window, viewport, camTransform))
    window.flip()

    t += Constants.dt

    if((Sys.getTime() * 1000) / Sys.getTimerResolution() - lastFPS > 1000) {
      Display.setTitle("FPS: " + fps)
      fps = 0;
      lastFPS += 1000
    }
    fps += 1
  }

  window.close()
}
