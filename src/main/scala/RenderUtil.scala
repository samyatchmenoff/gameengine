package com.yatchmenoff.gameengine

import org.lwjgl.BufferUtils
import com.yatchmenoff.gameengine.math.Matrix3x3

object RenderUtil {
  def matrixBuffer(m: Matrix3x3) = {
    val buf = BufferUtils.createFloatBuffer(16).put(Array(
        m.v0.x, m.v0.y, 0, m.v0.z,
        m.v1.x, m.v1.y, 0, m.v1.z,
        0,      0,      1,      0,
        m.v2.x, m.v2.y, 0, m.v2.z
      ))
    buf.flip
    buf
  }
}
