package com.yatchmenoff.gameengine

import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.newdawn.slick
import com.yatchmenoff.gameengine.math._

sealed trait FontStyle
object FontStyle {
  case object Plain extends FontStyle
  case object Bold extends FontStyle
  case object Italic extends FontStyle
  case object BoldItalic extends FontStyle
}

object Font {
  import FontStyle._
  def apply(name: String, style: FontStyle, size: Int) = {
    val awtStyle = style match {
      case Plain => java.awt.Font.PLAIN
      case Bold => java.awt.Font.BOLD
      case Italic => java.awt.Font.ITALIC
      case BoldItalic => java.awt.Font.BOLD | java.awt.Font.ITALIC
    }
    new Font(new slick.UnicodeFont(new java.awt.Font(name, awtStyle, size)))
  }
}

class Font(val slickFont: slick.UnicodeFont) {
  initialize()

  val scale = 1.0f / slickFont.getAscent()
  val height = slickFont.getAscent + slickFont.getDescent

  private[this] def initialize() = {
    import scala.collection.JavaConverters._
    val effects = slickFont.getEffects().asScala.asInstanceOf[scala.collection.mutable.Buffer[slick.font.effects.Effect]]
    effects += new slick.font.effects.ColorEffect(java.awt.Color.white)
    slickFont.addAsciiGlyphs()
    slickFont.loadGlyphs()
  }
}

object Text {
  sealed trait Alignment
  object Alignment {
    case object LeftTop extends Alignment
    case object CenterTop extends Alignment
    case object RightTop extends Alignment
    case object LeftBottom extends Alignment
    case object CenterBottom extends Alignment
    case object RightBottom extends Alignment
  }
}

class TextRenderer {
  def renderText(text: String, font: Font, transform: Matrix3x3, alignment: Text.Alignment): Unit = {
    glEnable(GL_TEXTURE_2D)
    glPushMatrix()
    glMultMatrix(RenderUtil.matrixBuffer(transform))
    glScalef(font.scale, -font.scale, 1.0f)
    val (posX, posY) = alignment match {
      case Text.Alignment.LeftTop => (0, 0)
      case Text.Alignment.CenterTop => (-font.slickFont.getWidth(text)/2, 0)
      case Text.Alignment.RightTop => (-font.slickFont.getWidth(text), 0)
      case Text.Alignment.LeftBottom => (0, -font.slickFont.getHeight(text))
      case Text.Alignment.CenterBottom => (-font.slickFont.getWidth(text)/2, -font.height)
      case Text.Alignment.RightBottom => (-font.slickFont.getWidth(text), -font.height)
    }
    font.slickFont.drawString(posX, posY, text)//, slick.Color.yellow)
    glPopMatrix()
  }
}

